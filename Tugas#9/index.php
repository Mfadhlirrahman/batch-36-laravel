<?php
require_once('animal.php');
require_once('Frog.php');
require_once('Ape.php');


   $sheep = new Animal("shaun");

    echo "Name : " . $sheep->Name . "<br>" ; // "shaun"
    echo "legs : "  . $sheep->legs . "<br>"; // 4
    echo "cold blooded : " . $sheep->cold_blooded . "<br> <br>"; // "no"

    $Frog = new Frog("Buduk");

    echo "Name : " . $Frog->Name . "<br>" ;
    echo "legs : "  . $Frog->legs . "<br>";
    echo "cold blooded : " . $Frog->cold_blooded . "<br>"; 
    echo " Jump : " .  $Frog->jump() ;
    
    $Ape = new Ape("Kera sakti");

    echo "Name : " . $Ape->Name . "<br>" ;
    echo "legs : "  . $Ape->legs . "<br>";
    echo "cold blooded : " . $Ape->cold_blooded . "<br>"; 
    echo " Yell : " .  $Ape->Yell();


?>