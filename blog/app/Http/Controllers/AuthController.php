<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
   public function daftar()

    {
       return view('halaman.form');
    }

    public function kirim(Request $request)
    {
        $firstname = $request['namadepan'];
        $lastname = $request['namabelakang'];

        return view('halaman.selamat', compact('firstname','lastname'));
    }
}
