<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeContoller@utama');
Route::get('/pendaftaran', 'AuthController@daftar');
Route::post('/kirim', 'Authcontroller@kirim');

Route::get('/table', function(){
    return view('patrial.table');
});
Route::get('/data-table', function(){
    return view('patrial.datatable');
});